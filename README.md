# AnyTextbookDownloader
亿方教材助手-用Python实现的电子教材下载器

哔哩哔哩关注“[小杨聊科技](https://space.bilibili.com/413043448)”或者在“[亿方运维专项站（正在维护）](https://yifang.yxxblog.top)”获取更多项目动态

演示视频：https://www.bilibili.com/video/BV1W2421M7pV/

背后的故事：https://mp.weixin.qq.com/s/Trf4iVhNRfSBZbVPMkZHqA

关键词：国家中小学智慧教育平台，智慧教育云，国家中小学智慧教育平台下载，国家中小学智慧教育平台403，国家中小学智慧教育平台下载报错，国家中小学教育云平台，电子教材，小学电子教材，初中电子教材，高中电子教材，电子教材下载器，人教版课本，人教版电子教材下载，统编版电子教材，统编版小学语文，统编版道德与法治，统编版初中语文，统编版初中政治，道德与法治，山东电子课本，五四制电子课本，鲁教版电子课本，鲁教版数学，鲁教版英语，日语课本，俄语课本，人教数学课本，人教英语课本，K12教育，双减政策，人教社，人教社电子教材，中小学电子教材，寒假，暑假，弯道超车，寒假预习，暑假预习，放假预习没有课本怎么办，去哪找电子教材，制作鬼畜课本，鬼畜课本怎么制作

## 公告

### 致歉声明

本UP主由于学业紧张，宣布暂时停更所有亿方软件，七月会继续更新，请大家谅解

向所有用户致歉。由于我们之前的一些版本无法兼容Windows 7系统和32位系统，导致部分用户无法安装使用，现已修复，对不起大家了！

### 新功能预告

七月份即将推出网页版亿方教材助手[全端版]。后续全部更新完预计七月完工。请大家拭目以待！

### 开源说明

本软件仅使用了合法的下载技术，本软件自身不存储任何课本资源，课本资源均来自国家的开放平台，若您认为本软件侵犯到了您的合法权益，请联系admin@yxxblog.top

在延伸的代码中（修改和由本仓库代码衍生的代码中）需要说明“基于 亿方教材助手（ https://gitlab.com/xiaoyangtech1/AnyTextbookDownloader/ ） 开发”。

## 使用方法

### Windows7及以上64位系统 傻瓜式运行办法（比这低的版本无法运行）

如果你是Windows，那么从Releases里面下载最新的Windows发行版，目前已在Windows 7 x64及以上操作系统测试通过

### 手动安装（适用于深度操作系统，统信UOS，openKylin等国产化Linux操作系统，Ubuntu，Debian等Linux发行版或者是傻瓜式运行行不通的场景）

#### 关于 Windows 7

Python 3.8.10 支持 Windows7 32位/64位 系统，是支持 Windows 7 系统的最后一个版本，3.9 以后开始不支持Windows 7 系统

#### 手动安装教程

UP主的测试环境是Windows 11 22H2 x64，使用Python 3.12.1 64-bit

首先安装好 Python3 64-bit ，如果你是Windows 7，看上面那一条安装Windows 7 兼容的版本

然后我们在Windows Powershell里面输入

`pip install -i https://mirrors.aliyun.com/pypi/simple/ BeautifulSoup4 lxml requests`

就完成基本配置了，可以跑起来用了

## Features

- [x] 批量下载“人民教育出版社”官网“教材”栏目任意一本教材的全部高清图片
- [x] 直接免登录下载“国家中小学智慧教育平台”中“教材”栏目的任一课本PDF（大部分为可复制可编辑，有水印可以使用“Adobe Acrobat Pro”等软件去除，还有一部分不可编辑的为纯图PDF）
- [x] 自动识别教材标题，自动重命名
- [x] 有效解决403，无权限等报错
- [x] 100％开源。你在GitLab或者哔哩哔哩提Issue，我们立马修复

## To Do

- [ ] 去除可编辑的出版社水印
- [ ] 对接安卓版“智慧中小学”下载API
- [ ] 更多网站的教材下载（需要啥网站可以在issue里提出，我会让这个程序变得更好）

## 此项目使用的API

### 智慧教育平台

#### 链接格式：

`https://basic.smartedu.cn/tchMaterial/detail?contentType=assets_document&contentId=一段字符&catalogType=tchMaterial&subCatalog=tchMaterial`

#### 解析接口：
标题：
`https://s-file-2.ykt.cbern.com.cn/zxx/ndrv2/resources/tch_material/details/{content_id}.json`

课本PDF：

部分课本下载的接口：`https://r3-ndr.ykt.cbern.com.cn/edu_product/esp/assets_document/{content_id}.pkg/pdf.pdf`

全部课本下载的接口（解决403）：`https://r2-ndr.ykt.cbern.com.cn/edu_product/esp/assets/{content_id}.pkg/pdf.pdf`

#### 示例课本网页链接：小学道法一上

`https://basic.smartedu.cn/tchMaterial/detail?contentType=assets_document&contentId=bdc00134-465d-454b-a541-dcd0cec4d86e&catalogType=tchMaterial&subCatalog=tchMaterial` 

### 人教社图片

#### 链接格式：

`https://book.pep.com.cn/数字ID/mobile/index.html`

#### 解析接口：

`https://book.pep.com.cn/数字ID/files/mobile/页码.jpg`

#### 示例课本网页链接：初中语文八上

`https://book.pep.com.cn/1311001201192/mobile/index.html`
